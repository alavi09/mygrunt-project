**My Grunt Project/Files**

This grunt setup automatically converts sass and less to css. It also livereloads html files.

Directory structure should be like this;
```
|--root
  |--less
  |--sass
  |--index.html*
  ```
*file not included in repo

First, install dependencies;

<code>npm install

To run;

<code>grunt

Sass and less files are compiled to their respectful folders, so for example style.scss must reside in /sass/ to be watched and converted on save.

Default server runs on ```localhost:9000.```To allow livereload(via grunt-contrib-connect) you must include the fallowing in your html before```</body>```;

```<script src="//localhost:35729/livereload.js"></script>```

Alternatively you can use the livereload browser extension [livereload browser extension](http://feedback.livereload.com/knowledgebase/articles/86242-how-do-i-install-and-use-the-browser-extensions- "Title").
